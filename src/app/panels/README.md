# App Overview

So basically, I want an input, output type app wherein 
the output is connected transfer function of an input. 


## Transfer Function Basic Principles

How does a transfer function work?

At least a first order transfer function can be structured as:

G(s) = Kp / (tau s + 1.0)

Where tau is the process time, Kp is the gain. And of course, s is the 
complex frequency of the system.

Based on laplace transform of b exp(-at),


G(s) = b / ( s + a)
G(s) = b/a / (1/a * s + 1.0)

Hence the process gain Kp is b/a and the process time tau is 1/a. 

Hence, if we were to give a process input, it will be scaled by Kp 
with process time tau.

## Programming Logic for First Order Transfer Function

The laplace function takes in a time domain input I(t) at time t, and 
will give an output O(t) at time t. 

If we consider a step input at time t1, with input I1(t1), then the 
output is simply. For an transfer function inverse laplaced 
to with gain b, and time constant a:


O(t) = I1(t1) * b * [1-exp(-a * [t-t1]])

If we use heaviside notation:

O(t) = u1(t - t1) * b * [1-exp(-a * [t-t1]])

In this case, the process gain is b, and we still need to figure out 
a. But that is a problem for later.

Now, suppose a second output comes in, at t2, the output will be:

O(t) = u1(t - t1) * b * [1-exp(-a * [t-t1]])
+ u2(t - t2) * b [1-exp(-a * [t-t2])]

Now, in the long run, as t goes to infinity, the output tends to 
u1 * b and u2 * b. This is a little problematic programmatically 
speaking because we will just have a vector of component outputs 
that we must add to obtain the system output over time.

So upon expiry, we must add the steady state vectors to some offset.


O(t) = u1(t - t1) * b * [1-exp(-a * [t-t1]])
+ u2(t - t2) * b [1-exp(-a * [t-t2])] + offset

when t - t1 is large enough, the [1 - exp( -a * [t-t1])] 
tends to 1, then we can add u1 * b to the offset:

```rust 
let mut offset: f64 = 0.0;
offset = offset + u1 * b;
```
Thus we obtain:

O(t) = u2(t - t2) * b [1-exp(-a * [t-t2])] + offset

The offset can be nested in a struct representing the transfer function.
Whereas, the inputs still undergoing time integrals can be placed in 
a vector. We could call each function a first order response or something.
We can also do first order response plus delay. That will be simply 
adding some delay factor to the subtraction we did earlier.
We can also add a boolean to each of these FirstOrderResponse objects 
to indicate if it has pretty much reached steady state. If so, then 
we can move simply move it to the offset.

As the expressions are already analytically integrated, we don't need 
any special time stepping specifically. So no worries about numerical
integration.

## Process of detecting a change in input.

For the SISO system, the transfer function must be able to detect 
some input induced by the user. How does it do that?

At t = 0, let's say the input received by the transfer function is 
I(t=0) = 1. But at t = 2, we have a new input such that I(t=2) = 1.5.
Hence, there is a step change of 0.5 at that instant (t=2).

Programmatically speaking, we need to log in that the change occured 
at t = 2s. Secondly, we have an increase in input of 0.5. To log 
that the change occured at t = 2s is relatively trivial. At least 
compared to the input increase. 

For the input increase, we need to take the new input value, 1.5. And 
we need to subtract the old input value from the new input value to 
obtain the change in input. We then need to register that we have a 
new FirstOrderResponse that we must add to the vector of responses. 
Thereafter, we must change the steady state time input value to 1.5 
and discard 1.0.

Hence, we need attributes in the struct, or at least a method to call 
such that I want a new input. Something like put in current input. 
The current input will be compared to the previous timestep input. If there 
is no change, then don't add to the vector, calculate as per normal.
Otherwise, add to the vector and calculate as per normal.


# Higher Order Transfer Functions

So assuming first order transfer functions are okay (they are at the 
time of writing), we want to deal with higher order transfer functions.
Something like:

G(s) = 2s / (1 + 7s + s^2)

We shall need other forms of transfer functions first, the sines and 
cosines, as well as the repeated linear factor sorts, which is a simple 
nth order type transfer function.

For simple sines and cosines...

We can look at sin (omega * t):

G(s) = omega / (s^2 + omega^2)

And cosine (omega * t): 

G(s) = s / (s^2 + omega^2)

The other building block we need is an nth order laplace transform...
Though to be fair, just placing first order filters would suffice.

For t^n exp (-at), the laplace transform is:

G(s) = n! / (s + a)^{n+1}

So it's essentially a multi filter kind of thing.

If we have an nth order transfer function, in the form: 

b/(s + a)^n

We need to convert it first to the factorial form:

b/(n-1)! * (n-1)! / (s+a)^n

We can turn it into the time domain:

b/(n-1)! t^(n-1) exp (-at)

It's a little bit hard to do it in terms of process time and 
process gain. But a is in units of Hz, n is a usize and b is 
a float. If we limit t to infinity, we get the steady state gain.
This is also limiting s to zero:

b/(n-1)! * (n-1)! / (a)^n

steady state gain: 

b / (a)^n

So we can write:


b/(a^n) * 1/(s * tau + 1)^n

I suppose the individual process time (tau) 
can likewise be thought of as 1/a. Now, let us reintroduce the n 
factorial:

b/(a^n) * 1/(n-1)! * (n-1)!/(s * tau + 1)^n


This is similar to before, but we get the effect of time out.




n is necessarily an integer of course.
For n=1, just return the FirstOrderTransferFn, or at least assert it is 
the same. Also, it's good to convert this into standard form as 
well.

Now, with these four building blocks, we should be able to convert 
any stable higher order transfer function into a sum of these 
transfer functions. It will of course, require the user to manually 
perform partial fractions. However, doing the partial fraction format 
is kind of cumbersome to do in code. I'll probably skip it for now.

## sine and cosine transfer functions (for underdamped systems)

Now for sines and cosines, the code is pretty straightforward.
There is no "decay" effect, so there's no need to store vectors. 
Everything goes straight into the offset value. Every impulse function 
goes into some offset value. In this case, vectors in 
the sines and cosines just keep growing and growing. One workaround 
of course, is to have the sines and cosines sum up.

The second, is to pair it up with decaying values. eg. first order with 
sine or cosine, or nth order with sine and cosine. This way, we can 
simulate an underdamped system with sine or cosine variations.





#![warn(clippy::all, rust_2018_idioms)]

mod app;
pub use app::GuiClient;
pub use app::first_order_transfer_fn;
pub use app::panels::opcua_panel;

